const axios = require('axios');

const config = require('../config')[process.env.NODE_ENV || 'development'];
const service = "stock-service";

class Cart {

    constructor() {
        this.products = []; 
    }

    async addProduct(product_desc) {
        try { 

            var res = await axios.get(`http://localhost:3000/find/${service}/${config.version}`);
            var ip = res.data.ip;
            var port = res.data.port;

            var product = await axios.get(`http://${ip}:${port}/${service}/${product_desc}`);

            // if it has stock add product
            if (product.data.stock != false) {
                this.products.push(product.data.stock);
            }

        } catch (err) {
            console.error(err);
        }
    }

    removeProduct(product_desc) { 

        try {  

            // this way we loop over products but use description to compare them
            this.products.forEach(product => {

                if(product.desc == product_desc) { 
                    
                    this.products.splice(this.products.indexOf(product), 1); 
                    this.total -= product.price;
                    return // is necesary for deleting just the first apperance and then return
                }
            }); 


        } catch (error) {
            console.log(error);
        }

        console.log(this.products);
        
    }

    toString() {
        var res = "Shopping Cart contains "  + this.products.length + " product/s: ";
        
        this.products.forEach(product => {
            res +=  product.desc + ", ";
        });
        return res;
    }
}

module.exports = Cart;
