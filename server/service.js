const express = require('express');
const Cart = require('../entities/cart')

const service = express();
const cart = new Cart();

module.exports = (config) => {
  const log = config.log();
  // Add a request logging middleware in development mode
  if (service.get('env') === 'development') {
    service.use((req, res, next) => {
      log.debug(`${req.method}: ${req.url}`);
      return next();
    });
  }

  service.get('/cart-service', (req, res, next) => {
    return res.json(cart.toString());
    //return next('Not implemented');
  });

  //put sería dos opciones una para añadir porductos y otra para eliminar
  service.put('/cart-service/add/:product', async (req, res, next) => { 
    const { product } = req.params;
    await cart.addProduct(product);
    return res.status(200).send();
  });
  
  //delete sería para eliminar todo el carrito
  service.put('/cart-service/del/:product', (req, res, next) => { 
    const { product } = req.params; 
    console.log(product)
    cart.removeProduct(product); 
    return res.status(200).send();
  });

  // eslint-disable-next-line no-unused-vars
  service.use((error, req, res, next) => {
    res.status(error.status || 500);
    // Log out the error to the console
    log.error(error);
    return res.json({
      error: {
        message: error.message,
      },
    });
  });
  return service;
};
